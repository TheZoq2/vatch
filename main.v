// RESET \033[0m
// RED \033[0;31m
// GREEN \033[0;32m

`define SETUP_TEST \
    initial begin \
        $dumpfile($sformatf("%s", `VCD_FILE)); \
        $dumpvars; \
    end \
    reg __failed = 0;

`define MAX__(x, y) ((x > y) ? x : y)

`define ASSERTION_ERROR(MSG) $write("["); \
    $write("\033[1;31m"); \
    $write("%m"); \
    $write("\033[0m"); \
    $write(": "); \
    $write("\033[1;36m"); \
    $write("%0d", `__LINE__); \
    $write("\033[0m"); \
    $write("]"); \
    $write("Assertion failed: %s", MSG);


// Compare expr with cond, reporting a failure if there is a diff. The printed
// values are padded with gray characters to fit max($bits(expr), $bits(cond)) + 1.
// The +1 is there because verilog doesn't like replicate 0 bits
`define ASSERT_EQ(expr, cond, MSG="") \
    if ((expr === cond) == 0) begin \
        __failed = 1; \
        `ASSERTION_ERROR(MSG); \
        $display("\n%s    Got:      %s%h%s%h /=\n%s    Expected: %s%h%s%h", \
            "\033[0;31m", \
            "\033[0;37m", \
            {(`MAX__($bits(expr), $bits(cond)) - $bits(expr)+1){1'b0}}, \
            "\033[0m", \
            expr, \
            "\033[0;32m", \
            "\033[0;37m", \
            {(`MAX__($bits(expr), $bits(cond)) - $bits(cond)+1){1'b0}}, \
            "\033[0m", \
            cond \
        ); \
    end


`define ASSERT_NE(expr, cond) \
    if ((expr !== cond) == 0) begin \
        __failed = 1; \
        `ASSERTION_ERROR; \
        $display("%h == %h", expr, cond); \
    end


`define END_TEST \
    if(__failed == 0) begin \
        $display("[\033[0;32m%m\033[0m] : All tests passed"); \
    end \
    else begin \
        $finish_and_return(1); \
    end \
    $finish(); \


`define WAIT_NEGEDGE(signal) \
    `ifdef __ICARUS__ \
    @(negedge signal) \
    `endif


// Generates a clock and a reset signal. Reset is asserted for `RESET_DURATION clock cycles
// and de-asserted on the negedge of the clock
`define CLK_AND_RST(CLK_NAME, RST_NAME, RESET_DURATION) \
    reg CLK_NAME; \
    reg RST_NAME = 1; \
    initial begin \
        CLK_NAME <= 0; \
        forever begin \
            #1 \
            CLK_NAME <= ~CLK_NAME; \
        end \
    end \
    initial begin \
        repeat(RESET_DURATION) @(negedge clk); \
        RST_NAME = 0; \
    end \
