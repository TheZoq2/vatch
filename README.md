A very small verilog test framework using macros.

# Usage

Define a test bench module 

```verilog
module sqrt_tb();
    // This must be called before anything else
    `SETUP_TEST

    initial begin
        // Optionally set up wave dumping
        $dumpfile(`VCD_OUTPUT);
        $dumpvars(0, sqrt_tb);
    end

    initial begin
        // Set up inputs
        in <= 1 << 12;
        // Wait for inputs to propagate
        @(negedge clk)
        // Check results
        `ASSERT_EQ(out, 1 << 12);
        `ASSERT_NE(out, 50);
        #10
        // When done. Invoke END_TEST
        `END_TEST
    end
endmodule
```
